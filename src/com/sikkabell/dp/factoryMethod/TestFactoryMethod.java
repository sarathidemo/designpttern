package com.sikkabell.dp.factoryMethod;

public class TestFactoryMethod {
	
	public static void main(String[] args) {
		// adding info and warn logger here
       CarCompanyFactory factory = new TataCompany();
       factory.buildVehicle("car");
		// adding info and warn logger here
       
       factory = new TeslCompany();
       factory.buildVehicle("car");
		// adding info and warn logger here
       
	}
}
