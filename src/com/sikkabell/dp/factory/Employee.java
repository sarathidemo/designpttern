package com.sikkabell.dp.factory;

public class Employee {

	private Integer id;
	private String name;
	private String address;
	private String msgType;
	
	public Employee() {
	//	super();
		// TODO Auto-generated constructor stub
	}
	 
	public Employee(Integer id, String name, String address, String msgType) {
		//super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.msgType = msgType;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getMsgType() {
		return msgType;
	}
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
	
	
}
