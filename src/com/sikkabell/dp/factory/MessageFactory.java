package com.sikkabell.dp.factory;

public class MessageFactory {

	public IMessage create(String msgType) {
		if(msgType.equalsIgnoreCase("whatsApp")) {
			return new WhatAppMessage();
		}else if(msgType.equalsIgnoreCase("facebook")) {
			return new FacebookMessage();
		}else {
			return null;
		}
	}
}
