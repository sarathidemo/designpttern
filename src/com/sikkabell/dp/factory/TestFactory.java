package com.sikkabell.dp.factory;

import java.util.ArrayList;
import java.util.List;

public class TestFactory {
	//add logger for tracing workflow
	
	static List<Employee> list = null;
	static {
		list = new ArrayList<>();
		list.add(new Employee(101, "Mark", "US", "whatsApp"));
		list.add(new Employee(101, "Jhon", "UK", "facebook"));
		list.add(new Employee(101, "Jet", "Canada", "facebook"));
		list.add(new Employee(101, "Jeo", "Japan", "whatsApp"));
		
		
	}
public static void main(String[] args) {
	MessageFactory message = new MessageFactory();

	
	list.stream().forEach(e->{
		IMessage msg = message.create(e.getMsgType());		
		msg.send();
	});

}
}
